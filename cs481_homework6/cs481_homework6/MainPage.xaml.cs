﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using cs481_homework6.Models;
using System.Diagnostics;
using Xamarin.Essentials;

//Joel Antony
//CS481
//HW6
//Resources used: Powerpoint slides, tutorialspoint, xamarin docs, stackoverflow
namespace cs481_homework6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()

        {
            InitializeComponent();

        }

        //This function is called when the user clicks the search button and searches for the word
        private async void SearchButtonClicked(object sender, EventArgs e)
        {
            //Initialize variable to hold user entry
            var userEntry = UserWord.Text;

            //Checks internet connection and returns no values if there is none
            //Sets rest of data to empty
            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
            {
                errorWord.Text = "Error. Unable to connect to the internet";
                currentword.Text = "";
                WordDefinition.Text = "";
                WordType.Text = "";
                WordExample.Text = "";
                return;
            }

            //Checks to see if user input is empty, returns error message if null/empty
            //Sets rest of data to empty
            if (string.IsNullOrEmpty(userEntry))
            {
                errorWord.Text = "Error. Please enter a word!";
                currentword.Text = "";
                WordDefinition.Text = "";
                WordType.Text = "";
                WordExample.Text = "";
                return;
            }
           
            //Set up the link and usage for owlbot API
            string link = "https://owlbot.info/api/v2/dictionary/" + userEntry; 
            //Create a new http client
            HttpClient client = new HttpClient();
            //Link for the uri
            var uri = new Uri(link);
            //Create the request message for get
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = uri
            };

            //Retrieve the response from the client request
            HttpResponseMessage response = await client.SendAsync(request);

            //if the status is OK, then provide the necessary information of the word from the JSON
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var content = await response.Content.ReadAsStringAsync();
                Definition[] words = Definition.FromJson(content);

                //no error message
                errorWord.Text = "";
                //provide the word
                currentword.Text = "Word: " + userEntry;
                //provide the definition of the word
                WordDefinition.Text = "Definition: " + words[0].DefinitionDefinition;
                //provide the type of the word
                WordType.Text = "Type: " + words[0].Type;
                //provide an example of the word
                WordExample.Text = "Example: " + words[0].Example;
            }
            //if the word is not found, provide a not found error code
            else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                //create the error message
                errorWord.Text = "Error. No corresponding word.";
                //rest of the word properties will be cleared
                currentword.Text = "";
                WordDefinition.Text = "";
                WordType.Text = "";
                WordExample.Text = "";
            }
            //If a bad request was retrieved, then provide appropriate error message
            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                //create the error message
                errorWord.Text = "Error. Incorrect data was retrieved.";
                //rest of the word properties will be cleared
                currentword.Text = "";
                WordDefinition.Text = "";
                WordType.Text = "";
                WordExample.Text = "";
            }
        }

        //Checks to see if the internet connection was changed, if it was, provide error message
        private void Connectivity_ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            if(e.NetworkAccess != NetworkAccess.Internet)
            {
                DisplayAlert("Error!", "Internet connection disrupted, please reconnect!", "Ok");
            }
        }
    }
}
